package com.peterchaschke.com.reference;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;

@SpringBootApplication(exclude= {UserDetailsServiceAutoConfiguration.class})
public class ReferenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReferenceApplication.class, args);
	}

}
