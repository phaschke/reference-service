package com.peterchaschke.com.reference.exchangeRates;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ExchangeRateRepository extends JpaRepository<ExchangeRates, Long>{
}
