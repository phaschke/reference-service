package com.peterchaschke.com.reference.exchangeRates;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDateTime;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@EnableScheduling
public class ExchangeRatesServiceImpl implements ExchangeRatesService {
	
	@Autowired
	private ExchangeRateRepository repository;
	
	@Value("${api.token.exchange-rate}")
    private String exchangeRateAPIKey;
	
	@Value("${api.exchange-rate.url}")
	private String exchangeRateAPIUrl;
	
	@Transactional
	@Scheduled(cron = "${cron.get-exchange-rates}")
	public ExchangeRates updateExchangeRates() {
		
		System.out.println("in get data");
		
		String data = callExchangeRateAPI();
		ExchangeRates exchangeRates = new ExchangeRates();
		exchangeRates.setId(1l);
		exchangeRates.setUpdateTime(LocalDateTime.now());
		exchangeRates.setData(data);
		
		return repository.save(exchangeRates);
	}
	
	@Transactional
	public ExchangeRates getExchangeRates() {
		
		ExchangeRates rates = null;
		
		// Check if exchange rate exists in the db, if not, try to get it
		if(!repository.existsById(1l)) {
			rates = updateExchangeRates();
			//return updateExchangeRates();
		} else {
			System.out.println("do not need to get data");
			rates = repository.getReferenceById(1l);
			//return repository.getReferenceById(1l);
		}
		
		JSONObject jsonRates = new JSONObject(rates.getData());
		JSONObject rebuiltObject = new JSONObject();
		rebuiltObject.put("conversion_rates", jsonRates.get("conversion_rates"));
		rebuiltObject.put("last_update", jsonRates.get("time_last_update_utc"));
		
		rates.setData(rebuiltObject.toString());
		
		return rates;
	}
	
	private String callExchangeRateAPI() {
		
		HttpRequest request = HttpRequest.newBuilder()
			.uri(URI.create(exchangeRateAPIUrl))
			.method("GET", HttpRequest.BodyPublishers.noBody())
			.build();
		HttpResponse<String> response = null;
		try {
			response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println(response.body());
		return response.body();
	}

}
