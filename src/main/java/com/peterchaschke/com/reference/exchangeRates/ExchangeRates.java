package com.peterchaschke.com.reference.exchangeRates;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "exchange_rates")
@Getter
@Setter
@RequiredArgsConstructor
public class ExchangeRates {
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@Column(name = "update_time")
	private LocalDateTime updateTime;
	
	@Column(name = "data")
	private String data; 
}
