package com.peterchaschke.com.reference.exchangeRates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/api/reference/exchange_rates")
public class ExchangeRatesController {
	
	@Autowired
	ExchangeRatesService service;
	
	@GetMapping("")
	public ResponseEntity<String> getExchangeRates() {
		
		ExchangeRates er = service.getExchangeRates();
		ExchangeRatesResource resource = new ExchangeRatesResource();
		resource.setData(er.getData());
		
		return new ResponseEntity<String>(resource.getData(), HttpStatus.OK);
	}
}
