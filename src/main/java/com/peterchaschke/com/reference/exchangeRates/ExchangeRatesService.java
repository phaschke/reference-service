package com.peterchaschke.com.reference.exchangeRates;

public interface ExchangeRatesService {
	
	public ExchangeRates updateExchangeRates();
	public ExchangeRates getExchangeRates();
	
}
