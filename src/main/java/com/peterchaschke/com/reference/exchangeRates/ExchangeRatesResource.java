package com.peterchaschke.com.reference.exchangeRates;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExchangeRatesResource {
	
	private int id;
	private String updateTime;
	private String data;


}
